package org.emc.debug;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.main.bootstrap.Bootstrap;
import me.deftware.client.framework.main.bootstrap.discovery.AbstractModDiscovery;
import me.deftware.mod.Main;
import net.fabricmc.api.ModInitializer;

import java.io.File;
import java.io.InputStreamReader;

@SuppressWarnings("ConstantConditions")
public class FabricLauncher implements ModInitializer {

	@Override
	public void onInitialize() {
		try {
			JsonObject json = new Gson().fromJson(new InputStreamReader(FabricLauncher.class.getResourceAsStream("/client.json")), JsonObject.class);
			AbstractModDiscovery.AbstractModEntry modEntry = new AbstractModDiscovery.AbstractModEntry(new File("./mod.jar"), json) {
				@Override
				public void init() { }

				@Override
				public EMCMod toInstance() throws Exception {
					return new Main();
				}
			};
			Bootstrap.loadMod(modEntry);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
