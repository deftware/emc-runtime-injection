package me.deftware.mod;

import me.deftware.client.framework.main.EMCMod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main extends EMCMod {

    public static Main INSTANCE;
    private Logger LOGGER;

    @Override
    public void initialize() {
        INSTANCE = this;
        LOGGER = LogManager.getLogger(this.getMeta().getName());
        LOGGER.info("Initializing {} v{}...", this.getMeta().getName(), this.getMeta().getVersion());
    }

    @Override
    public void postInit() {
        LOGGER.info("Initialized {}", this.getMeta().getName());
    }

}
